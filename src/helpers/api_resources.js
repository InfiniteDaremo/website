import { post, del, get, put } from "./api_helper"
import * as url from "./url_helper"

export const getCareer = () => get(url.GET_CAREER)
