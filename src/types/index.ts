export interface AppState {
    Career: any;
    Layout: any;
}

export interface Job {
    company:      string;
    role:         string;
    icon:         string;
    summary:      string;
    dates:        Dates;
    achievements: string[];
}

export interface Dates {
    start: number;
    end?:   number;
}
