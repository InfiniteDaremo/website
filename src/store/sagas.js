import { all } from "redux-saga/effects"

import LayoutSaga from "./layout/saga"
import CareerSaga from "./career/saga"

export default function* rootSaga() {
  yield all([LayoutSaga(), CareerSaga()])
}
