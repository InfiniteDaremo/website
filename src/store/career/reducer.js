import { GET_CAREER_SUCCESS } from "./action_types"

const initialState = {
  career: [],
}

const career = (state = initialState, action) => {
  switch (action.type) {
    case GET_CAREER_SUCCESS:
      return {
        ...state,
        career: action.payload,
      }
    default:
      state = { ...state }
      break
  }
  return state
}

export default career
